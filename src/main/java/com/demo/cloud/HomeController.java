package com.demo.cloud;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;


 

@Configuration
@EnableApolloConfig({"application"})
@RestController
public class HomeController {
   
    @Value("${APP_VERSION}")
    private String  appVersion;
 
   @RequestMapping("/")
    public String hello() {
        return "Welcome to Demo App version " + appVersion + " !";
    }
}
