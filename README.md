# DemoProject
这个demo是一个 基于maven构建的 spring boot工程。期望通过该工程完成几个验证
1.基于svn或git的代码仓库提交后自动构建
2.通过提交注释与jira关联
3.通过HomeController类的APP_VERSION实现配置文件的验证（包括 ：多环境和 版本等）
4.工程代码扫描（分析：jar依赖和版本、漏洞扫描等）
5.和编写的基于api接口的自动化测试进行集成。
